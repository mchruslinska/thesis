Table of contents:
1. Introduction
2. Double neutron stars: merger rates revisited, M. Chruślińska et al., MNRAS, 474, 3 (2018)
3. The influence of the distribution of cosmic star formation at different metallicities on the properties of merging double compact objects, M. Chruślińska, G. Nelemans & K. Belczyński, MNRAS, 482, 4 (2019)
4. Metallicity of stars formed throughout the cosmic history based on the observational properties of star forming galaxies, M. Chruślińska & G. Nelemans, MNRAS, 488, 4 (2019)
5. The effect of the environment-dependent IMF on the formation and metallicities of stars over the cosmic history, M. Chruślińska et al., A&A, 636, A10 (2020)
6. The impact of the FMR and starburst galaxies on the (low-metallicity) cosmic star formation history, M. Chruślińska et al.,submitted to MNRAS
7. Summary
8. Research Data Management Statement
